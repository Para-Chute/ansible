## Tp DevOps ansible-202 by Clément JELEFF ##

Comment j'ai procédé à la réalisation de ce projet 

1)Création des machines AWS

2)Initialisation des fichiers de config du projet (ajouter les IP's des machines et définir les rôles des machines)

3) Rechercher les moyens de réaliser ce projet; créer un serveur web LAMP semble etre la meilleur solution d'apres les forums.
J'ai donc suivi un tuto d'instalation de serveur LAMP jusqu'au moment ou je me suis rendu compte que cela ne convenais pas au besoin du TP.

4) J'ai conclu qu'il fallait les modules suivants poour le bon fonctionement du TP:

- appache2
- mysql-server
- php7.0-common
- php7.0-mysql
- libapache2-mod-php7.0
- python-mysqldb
- wget

5) Création chaques dossier et atribué tous les roles aux deux serveurs AWS

6) J'ai pris la liberté d'utiliser Caddy car ce dernier permet de mieux sécuriser ses pages en activant par défaut le HTTPS


## Lancer le serveur ANSIBLE ##

1) Dans un terinal ubuntu se rendre dans le chemin du projet, dans mon cas :
root@Para-Chute:/mnt/c/Users/cleme/Desktop/Projet_ansible#

2) Saisir : "export ANSIBLE_CONFIG=~/ansible.cfg" 

3) Lancer la commande "ansible-playbook -i inventory.ini -u"

4) Saisir l'IP de la machine WEB dans son nagigateur, dans notre cas : 54.86.29.89